import {
  Avatar as MuiAvatar,
  Card,
  CardContent,
  Container,
  Grid,
  styled,
  Typography,
} from "@mui/material";
import Button from "components/Button";
import HtmlMeta from "components/HtmlMeta";
import { EmailIcon, GlobeIcon, LinkedInIcon, PinIcon } from "components/Icons";
import IconText from "components/IconText";
import PageTitle from "components/PageTitle";
import StyledLink from "components/StyledLink";
import Link from "next/link";
import { volunteerRoute } from "routes";

import TeamData from "./team.json";

const SpacerDiv = styled("div")(({ theme }) => ({
  height: theme.spacing(4),
}));

const TeamMemberCard = styled(Card)(({ theme }) => ({
  height: "100%",
}));

const TeamMembedCardContent = styled(CardContent)(({ theme }) => ({
  display: "flex",
}));

const DetailDiv = styled("div")(({ theme }) => ({
  marginTop: theme.spacing(1),
  marginBottom: theme.spacing(1),
  marginLeft: theme.spacing(2),
  marginRight: theme.spacing(2),
  flex: "1 0 auto",
}));

const StyledAvatar = styled(MuiAvatar)(({ theme }) => ({
  width: theme.typography.pxToRem(96),
  height: theme.typography.pxToRem(96),
}));

export default function Team() {
  return (
    <>
      <HtmlMeta title="The Team" />
      <Container maxWidth="md">
        <PageTitle>The Team</PageTitle>
        <Typography paragraph>
          We are all couch surfers and skilled professionals who want to build
          an improved, safer and more inclusive platform that can support and
          sustainably grow the couch surfing community and bring its values to
          the world. If you feel the same way and want to contribute, then we'd
          love to talk to you.
        </Typography>
        <Typography paragraph>
          <Link href={volunteerRoute} passHref legacyBehavior>
            <Button variant="contained" color="secondary">
              Join the team
            </Button>
          </Link>
        </Typography>
      </Container>
      <SpacerDiv />
      <section>
        <Grid
          container
          maxWidth="xl"
          spacing={2}
          justifyContent="center"
          alignItems="stretch"
        >
          {TeamData.map(
            ({ name, director, board_position, role, location, img, link }) => (
              <Grid key={name} item xs={12} md={6} lg={4}>
                <TeamMemberCard elevation={director ? 3 : 1}>
                  <TeamMembedCardContent>
                    <StyledAvatar alt={`Headshot of ${name}`} src={img} />
                    <DetailDiv>
                      <Typography
                        variant={director ? "h1" : "h2"}
                        component="h2"
                      >
                        {name}
                      </Typography>
                      {director && (
                        <Typography variant="h2" component="h3">
                          {board_position}
                        </Typography>
                      )}
                      <Typography variant="h3">{role}</Typography>
                      <IconText icon={PinIcon} text={location} />
                      {link && (
                        <IconText
                          icon={
                            link.type === "linkedin"
                              ? LinkedInIcon
                              : link.type === "email"
                                ? EmailIcon
                                : GlobeIcon
                          }
                          text={
                            <Typography variant="body1">
                              <StyledLink href={link.url}>
                                {link.text}
                              </StyledLink>
                            </Typography>
                          }
                        />
                      )}
                    </DetailDiv>
                  </TeamMembedCardContent>
                </TeamMemberCard>
              </Grid>
            ),
          )}
        </Grid>
      </section>
      <SpacerDiv />
      <Container maxWidth="md">
        <Typography variant="h2" component="h2">
          Have skills you want to contribute?
        </Typography>
        <Typography paragraph>
          Couchers.org is a community project, built by folks like you for the
          benefit of the global couch surfing community. If you would like to be
          a part of this great new project, or leave your feedback on our ideas,
          click the button below and fill out the short form.
        </Typography>
        <Typography paragraph>
          <Link href={volunteerRoute} passHref legacyBehavior>
            <Button variant="contained" color="secondary">
              Join our team
            </Button>
          </Link>
        </Typography>
      </Container>
    </>
  );
}
